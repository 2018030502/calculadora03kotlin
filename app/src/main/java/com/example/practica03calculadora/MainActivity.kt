package com.example.practica03calculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.content.Intent
import com.example.practica03calculadora.CalculadoraActivity


class MainActivity : AppCompatActivity() {
    private lateinit var btnLogin: Button
    private lateinit var btncerrar: Button
    private lateinit var txtUsuario: EditText
    private lateinit var txtContraseña: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnLogin.setOnClickListener { login() }
        btncerrar.setOnClickListener {
            salir()
        }
    }

    private fun iniciarComponentes() {
        btnLogin = findViewById(R.id.btnLogin)
        btncerrar = findViewById(R.id.btncerrar)
        txtContraseña = findViewById(R.id.txtContraseña)
        txtUsuario = findViewById(R.id.txtUsuario)
    }

    private fun login() {
        val strUsuario: String = resources.getString(R.string.usuario)
        val strContra: String = resources.getString(R.string.contraseña)

        if (txtUsuario.text.toString() == strUsuario && txtContraseña.text.toString() == strContra) {
            val intent = Intent(this@MainActivity, CalculadoraActivity::class.java)
            intent.putExtra("usuario", strUsuario)
            startActivity(intent)
        } else {
            Toast.makeText(this, "Usuario o contraseña no válida", Toast.LENGTH_LONG).show()
        }
    }
    private fun salir() {
        finish()
    }
}
