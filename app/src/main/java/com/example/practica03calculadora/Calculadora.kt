package com.example.practica03calculadora

class Calculadora( var num1: Int,  var num2: Int) {
    fun suma(): Int {
        return num1 + num2
    }

    fun resta(): Int {
        return num1 - num2
    }

    fun multiplicacion(): Int {
        return num1 * num2
    }

    fun division(): Int {
        if (num2 != 0) {
            return num1 / num2
        }
        throw ArithmeticException("No se puede dividir entre cero.")
    }
}