package com.example.practica03calculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Button
import android.widget.TextView
import android.app.AlertDialog
import com.example.practica03calculadora.Calculadora





class CalculadoraActivity : AppCompatActivity() {
    private lateinit var btnSuma: Button
    private lateinit var btnResta: Button
    private lateinit var btnMulti: Button
    private lateinit var btnDividir: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    private lateinit var txtnum1: EditText
    private lateinit var txtnum2: EditText
    private lateinit var lblResultado: TextView
    private lateinit var Calculadora: Calculadora

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)

        iniciarComponentes()

        btnSuma.setOnClickListener { btnSuma() }
        btnResta.setOnClickListener { btnResta() }
        btnMulti.setOnClickListener { btnMulti() }
        btnDividir.setOnClickListener { btnDividir() }
        btnLimpiar.setOnClickListener { limpiar() }
        btnRegresar.setOnClickListener { regresar() }




    }

    private fun iniciarComponentes() {
        btnSuma = findViewById(R.id.btnSuma)
        btnResta = findViewById(R.id.btnResta)
        btnDividir = findViewById(R.id.btnDividir)
        btnMulti = findViewById(R.id.btnMulti)
        txtnum1 = findViewById(R.id.txtnum1)
        txtnum2 = findViewById(R.id.txtnum2)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnregresar)

        lblResultado = findViewById(R.id.lblresultado)
        Calculadora = Calculadora(0, 0)
    }

    private fun btnSuma() {
        Calculadora.num1 = txtnum1.text.toString().toInt()
        Calculadora.num2 = txtnum2.text.toString().toInt()
        lblResultado.text = Calculadora.suma().toString()
    }

    private fun btnResta() {
        Calculadora.num1 = txtnum1.text.toString().toInt()
        Calculadora.num2 = txtnum2.text.toString().toInt()
        lblResultado.text = Calculadora.resta().toString()
    }

    private fun btnDividir() {
        Calculadora.num1 = txtnum1.text.toString().toInt()
        Calculadora.num2 = txtnum2.text.toString().toInt()
        lblResultado.text = Calculadora.division().toString()
    }

    private fun btnMulti() {
        Calculadora.num1 = txtnum1.text.toString().toInt()
        Calculadora.num2 = txtnum2.text.toString().toInt()
        lblResultado.text = Calculadora.multiplicacion().toString()
    }

    private fun limpiar() {
        txtnum1.setText("")
        txtnum2.setText("")
        lblResultado.setText("")
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("¿Deseas regresar?")
        confirmar.setPositiveButton("Confirmar") { _, _ -> finish() }
        confirmar.setNegativeButton("Cancelar", null)
        confirmar.show()
    }



}
